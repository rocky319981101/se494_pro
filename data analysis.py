# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 10:49:14 2020

@author: Lucy
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns 

# 读取文件
filename = r'C:\Users\Lucy\Desktop\vgsales.csv'

data = pd.read_csv(filename)

# plt.plot(data.positive_ratings,data.Global_Sales,'ro')

# 好评数对数化
data['positive_ratings2'] = np.log(data.positive_ratings.fillna(0))

# 画图
plt.plot(data.positive_ratings2,data.Global_Sales,'ro')

# 选取部分数据
# data = data[data.positive_ratings>500]


from sklearn import linear_model
# 线性回归建模
# 自变量
x = pd.DataFrame(data.positive_ratings2)
# 因变量
y = data.Global_Sales.fillna(0)
# 建模
lrModel = linear_model.LinearRegression()
# 模型训练
lrModel.fit(x,y)
# 模型得分
lrModel.score(x,y)

# 截距b和斜度a
a = lrModel.coef_[0]
b = lrModel.intercept_

# 预测值
y_line = lrModel.predict(x)

# 画图
sns.lmplot(x='positive_ratings2',y='Global_Sales',data=data)
plt.savefig(r'C:\Users\Lucy\Desktop\vgsales.png')
