from django.shortcuts import render

# Create your views here.
def index(request):
    """主页"""
    return render(request, 'youxi/index.html')

def jieguo(request):
    """结果"""
    filename = r'K\pycharmproject\untitled1\templates\youxi\vgsales.png'
    context = {'filename':filename}
    return render(request, 'youxi/jieguo.html',context)