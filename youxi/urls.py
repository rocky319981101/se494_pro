from django.urls import path,include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from youxi import views

app_name='youxi'
urlpatterns = [
    path("", views.index),
    path("jieguo",views.jieguo,name='jieguo'),
]
urlpatterns += staticfiles_urlpatterns()
